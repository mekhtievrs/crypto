<?php

namespace Database\Seeders;

use Domains\Contact\Seeders\ContactSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call(ContactSeeder::class);
    }
}
