<?php

namespace Domains\Contact\src\Services;

use Domains\Contact\src\Models\Contact;
use Domains\Contact\src\Requests\ContactRequest;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class ContactService
{
    public function getPaginatedData(Request $request): LengthAwarePaginator
    {
        $itemsPerPage = $request->input('itemsPerPage', 10);
        return QueryBuilder::for(Contact::query())
            ->allowedSorts(Contact::getAllowedSorts())
            ->allowedFilters(Contact::getAllowedFilters())
            ->defaultSort('-created_at')
            ->paginate($itemsPerPage);
    }

    public function createFromRequest(ContactRequest $request)
    {
        return Contact::create($request->validated());
    }

    public function updateFromRequest(ContactRequest $request, Contact $contact): Contact
    {
        $contact->update($request->validated());
        return $contact;
    }

    public function deleteFromRequest(Contact $contact): ?bool
    {
        return $contact->delete();
    }
}
