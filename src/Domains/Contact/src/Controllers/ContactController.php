<?php

namespace Domains\Contact\src\Controllers;

use App\Http\Controllers\Controller;
use Domains\Contact\src\Models\Contact;
use Domains\Contact\src\Requests\ContactRequest;
use Domains\Contact\src\Resources\ContactResource;
use Domains\Contact\src\Services\ContactService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use function response;

class ContactController extends Controller
{
    public function __construct(
        protected ContactService $service,
    )
    {
    }

    public function index(Request $request): AnonymousResourceCollection
    {
        return ContactResource::collection(
            $this->service->getPaginatedData($request)
        );
    }

    public function show(Contact $contact)
    {
        return new ContactResource($contact);
    }

    public function store(ContactRequest $request): JsonResponse
    {
        $model = $this->service->createFromRequest($request);
        return (new ContactResource($model))->response()->setStatusCode(201);
    }

    public function update(ContactRequest $request, Contact $contact): JsonResponse
    {
        $model = $this->service->updateFromRequest($request, $contact);
        return (new ContactResource($model))->response()->setStatusCode(201);
    }

    public function destroy(Request $request, Contact $contact): JsonResponse
    {
        $this->service->deleteFromRequest($contact);
        return response()->json([], 204);
    }
}
