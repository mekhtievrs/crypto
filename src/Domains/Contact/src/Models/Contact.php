<?php

namespace Domains\Contact\src\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'phone',
    ];

    public static function getAllowedSorts(): array
    {
        return [
            'created_at',
        ];
    }

    public static function getAllowedFilters(): array
    {
        return [];
    }
}
